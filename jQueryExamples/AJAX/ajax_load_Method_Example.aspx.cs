﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace jQueryExamples.AJAX
{
    public partial class ajax_load_Method_Example : System.Web.UI.Page
    {
        SqlConnection scon = null;
        SqlDataAdapter sda=null;
        DataSet ds = null;

        string sql = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            scon = new SqlConnection("Data Source=DESKTOP-8LJT91C;Database=Company;User Id=sa;Password=12345");
            if (!Page.IsPostBack)
                BindDeptDDl();
        }
       void BindDeptDDl()
        {
            sql = "Select * From Dept";
            sda = new SqlDataAdapter(sql, scon);
            ds = new DataSet();
            sda.Fill(ds, "Dept");
            ddl1.DataSource = ds.Tables["Dept"];
            ddl1.DataTextField = "DeptName";
            ddl1.DataValueField = "DeptId";
            ddl1.DataBind();
            ddl1.Items.Insert(0, new ListItem("All","0"));
        }
    }
}