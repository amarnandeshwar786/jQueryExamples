﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEmpData.aspx.cs" Inherits="jQueryExamples.AJAX.ViewEmpData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="gv1" runat="server" Caption="Employee Data" EmptyDataText="No Employee Data Available for This Selected Dept!!!" EmptyDataRowStyle-ForeColor="Red"></asp:GridView>
    </div>
    </form>
</body>
</html>
