﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ajax_load_Method_Example.aspx.cs" Inherits="jQueryExamples.AJAX.ajax_load_Method_Example" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../Scripts/jquery-3.1.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var deptId;
            function loadEmpData() {
                deptId = $("#ddl1 option:selected").val();

            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
    <b>Dept Name : </b> <asp:DropDownList ID="ddl1" runat="server"></asp:DropDownList>
        <div>
            <asp:Panel ID="pnl1" runat="server"></asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>
